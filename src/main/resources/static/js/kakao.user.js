function kakaoShare() {
	// kakao javascript api key
	Kakao.init('3c9db22b1943c80ae5eb6f858d2e8efe');
	Kakao.isInitialized();
    Kakao.Link.sendDefault({
      objectType: 'feed',
      content: {
        title: '카카오공유하기 시 타이틀',
        description: '카카오공유하기 시 설명입니다.',
        imageUrl: '카카오공유하기 시 썸네일 이미지 경로를 지정해주세',
        link: {
          mobileWebUrl: '카카오공유하기 시 클릭 후 이동 경로를 설정하세',
          webUrl: '카카오공유하기 시 클릭 후 이동 경로',
        },
      },
      buttons: [
        {
          title: '웹으로 보기',
          link: {
            mobileWebUrl: '카카오공유하기 시 클릭 후 이동 경로',
            webUrl: '카카오공유하기 시 클릭 후 이동 경로',
          },
        },
      ],
      // 카카오톡 미설치 시 카카오톡 설치 경로이동
      installTalk: true,
    })
}